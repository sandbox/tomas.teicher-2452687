-- INSTALLATION --

* Install as usual Drupal module, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --
1. This payment method can be configured as other payment methods. After enabling the module, mTransfer should appear in the list of payment methods,
(it is page /admin/commerce/config/payment-methods)
2. After clicking on "mTransfer" in the list, 'Editing reaction rule "mTransfer"'  page appears
(page admin/commerce/config/payment-methods/manage/commerce_payment_mtransfer)
mTransfer settings (ServiceID and offsite URL) can be edited in "Actions" table


-- HOW IT WORKS

When mTransfer payment method is enabled, it can be chosen by users as payment method during checkout process.
When customer choose mTransfer, user is redirected offsite to third-party (mTransfer) website, where the payment will be processed. 
After payment is processed, user is redirected from mTransfer website back to eshop to continue (and finish) checkout process.
After return to eshop, this module automatically update status of payment transaction. There are three various scenarios, how the payment was processed:
mTransfer does not use encrypted data, when sending response to eshop website. 
Therefore, this module cannot consider the response as trusted information, whether payment was successfully processed on mBank website.
When user pays on mBank website, this module does not change automatically status of payment transaction in Drupal database. 
Administrator of commerce website needs to check all payments in his/her bank account.





