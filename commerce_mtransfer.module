<?php

/**
 * @file
 * Implementation of mTransfer payment service into Drupal Commerce
 */
//Machine name for mTransfer payment method
define("COMMERCE_PAYMENT_METHOD_MTRANSFER", 'mtransfer');

/**
 * Implements hook_commerce_payment_method_info().
 * Define mTransfer payment method.
 */
function commerce_mtransfer_commerce_payment_method_info() {
  $payment_methods[COMMERCE_PAYMENT_METHOD_MTRANSFER] = array(
    'base' => COMMERCE_PAYMENT_METHOD_MTRANSFER,
    'title' => t('mTransfer'),
    'short_title' => t('mTransfer'),
    'description' => t('mTransfer'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );
  return $payment_methods;
}

/**
 * Implementation of payment method redirect callback
 * redirect form for mTransfer payment method
 * 
 * @param array $form
 * @param array $form_state
 * @param object $order
 * @param array $payment_method
 * @return array $form
 */
function mtransfer_redirect_form($form, &$form_state, $order, $payment_method) {
  $settings = $payment_method['settings'];

  $form['ServiceID'] = array(
    '#type' => 'hidden',
    '#value' => $settings['ServiceID']
  );

  $form['Description'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );

  $form['OrderId'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );

  //The only available currency is EUR.
  //The price is saved in minor units (cents). Get price in major unit. 
  $price = $order->commerce_order_total[LANGUAGE_NONE][0]['amount'] / 100;
  $form['Amount'] = array(
    '#type' => 'hidden',
    '#value' => $price
  );

  $form['TrDate'] = array(
    '#type' => 'hidden',
    '#value' => 'Now'
  );

  $form['#method'] = 'get';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  $form['#action'] = url($settings['live_url'], array('external' => TRUE));
  return $form;
}

/**
 * Implementation of hook_menu()
 * @return array $items
 */
function commerce_mtransfer_menu() {
  //RURL path parameter of mTransfer.
  $items['commerce-mtransfer-return'] = array(
    'title' => 'Return form offsite payment',
    'page callback' => 'commerce_mtransfer_return_page',
    'access callback' => '_commerce_mtransfer_return_page_access',
  );
  return $items;
}

/**
 * Access callback for mTransfer return page
 * @return boolean - return TRUE if current user has access to the page
 */
function _commerce_mtransfer_return_page_access() {
  if (empty($_GET['OrderId'])) {
    return FALSE;
  }
  $order = commerce_order_load($_GET['OrderId']);
  if (empty($order)) {
    return FALSE;
  }
  return commerce_order_access('update', $order);
}

/**
 * Page callback for RURL return page from mTransfer.
 * 
 * Since mTransfer sends information without any encryption, we cannot trust its parameters.
 * Therefore we don't edit orders after return, we just redirect user back to checkout process.
 */
function commerce_mtransfer_return_page() {
  //OrderId GET parameter shoul be always present in URL according mTransfer documentation, but we can check it.
  if (!empty($_GET['OrderId'])) {
    $order = commerce_order_load($_GET['OrderId']);
    $return_path = 'checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'];
  }
  else {
    $return_path = 'checkout';
  }
  drupal_goto($return_path);
}

/**
 * Payment method callback: mTransfer settings form
 * @param $settings
 * @return array $form
 */
function mtransfer_settings_form($settings = NULL) {
  $form = array();
  $init_payment_settings = array(
    'live_url' => 'https://sk.mbank.eu/mtransfer.asp',
    'ServiceID' => '',
  );

  $settings = (array) $settings + $init_payment_settings;


  $form['live_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Offsite URL'),
    '#description' => t('URL of mTransfer service on mBank website'),
    '#default_value' => $settings['live_url'],
  );

  $form['ServiceID'] = array(
    '#type' => 'textfield',
    '#title' => t('Service ID'),
    '#required' => TRUE,
    '#default_value' => $settings['ServiceID'],
    '#description' => t('Merchant ID provided by payment service'),
    '#size' => 8,
  );

  return $form;
}
